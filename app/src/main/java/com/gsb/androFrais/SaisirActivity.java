package com.gsb.androFrais;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class SaisirActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saisir);

        initFicheFrais();
    }

    private void initFicheFrais() {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;
        String mois = "" + month;
        if (mois.length() == 1) {
            mois = "0" + month;
        }
        final String moisAnnee = mois + calendar.get(Calendar.YEAR);

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://ws.gsb.com/appliFrais/FicheFrais",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray ficheFrais = jsonObject.optJSONArray("data");
                            JSONArray ligneFraisForfait = ficheFrais.getJSONObject(0).optJSONArray("Lignefraisforfaits");

                            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayoutFraisForfait);
                            linearLayout.setTag(ficheFrais.getJSONObject(0).optString("Idfichefrais"));
                            for (int i = 0; i < ligneFraisForfait.length(); i++) {
                                JSONObject ligne = ligneFraisForfait.getJSONObject(i);

                                TextView textView = new TextView(linearLayout.getContext());
                                String s = ligne.optString("Idfraisforfait");
                                textView.setText(s);
                                textView.setTextSize(20);

                                EditText editText = new EditText(linearLayout.getContext());
                                editText.setTag(ligne.optString("Idfraisforfait"));
                                String quantite = ligne.optString("Quantite");
                                editText.setText(quantite);

                                linearLayout.addView(textView);
                                linearLayout.addView(editText);
                            }

                            Button b = new Button(linearLayout.getContext());
                            b.setText(R.string.buttonValider);
                            b.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    LinearLayout view = (LinearLayout) findViewById(R.id.linearLayoutFraisForfait);
                                    for (int i = 0; i < view.getChildCount(); i++) {
                                        if (view.getChildAt(i) instanceof EditText) {
                                            EditText editText = (EditText) view.getChildAt(i);
                                            String quantite = editText.getText().toString();
                                            String idFraisForfait = editText.getTag().toString();
                                            update(quantite, idFraisForfait, view.getTag().toString());
                                        }
                                    }

                                }
                            });
                            linearLayout.addView(b);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String err = error.getMessage();
                        Log.e("MainActivity", err);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("moisAnnee", moisAnnee);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };//fin stringRequest*/
        queue.add(sr);

    }

    private void update(final String quantite, final String idFraisForfait, String idFicheFrais) {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.PUT,
                "http://ws.gsb.com/appliFrais/Lignefraisforfait?id=" + idFicheFrais,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.getMessage();
                Log.e("SaisirActivity", err);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("quantite", quantite);
                params.put("idFraisForfait", idFraisForfait);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };//fin stringRequest
        queue.add(sr);
    }


}
