package com.gsb.androFrais.classesMetier;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LigneFraisForfait {

    private String idFraisForfait;
    private int quantite;

    public String getIdFraisForfait() {
        return idFraisForfait;
    }

    public int getQuantite() {
        return quantite;
    }

    public LigneFraisForfait(JSONObject jsonObject){
        try {
            idFraisForfait = jsonObject.getString("Idfraisforfait");
            quantite = jsonObject.getInt("Quantite");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<LigneFraisForfait> jsonToArrayListObject(JSONArray jsonArray){
        ArrayList<LigneFraisForfait> collectionLigneFraisForfait = new ArrayList<LigneFraisForfait>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                collectionLigneFraisForfait.add(new LigneFraisForfait(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return collectionLigneFraisForfait;
    }
}
