package com.gsb.androFrais.classesMetier;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FicheFrais {

    private String idFicheFrais;
    private String moisAnnee;
    private int nbJustificatifs;
    private double montantValide;
    private long dateModif;
    private ArrayList<LigneFraisForfait> listeLigneFraisForfait;
    private ArrayList<LigneFraisHorsForfait> listeLigneFraisHorsForfait;

    public String getIdFicheFrais() {
        return idFicheFrais;
    }

    public String getMoisAnnee() {
        return moisAnnee;
    }

    public int getNbJustificatifs() {
        return nbJustificatifs;
    }

    public double getMontantValide() {
        return montantValide;
    }

    public long getDateModif() {
        return dateModif;
    }

    public ArrayList<LigneFraisForfait> getListeLigneFraisForfait() {
        return listeLigneFraisForfait;
    }

    public ArrayList<LigneFraisHorsForfait> getListeLigneFraisHorsForfait() {
        return listeLigneFraisHorsForfait;
    }

    public FicheFrais(JSONObject jsonObject) {
        idFicheFrais = jsonObject.optString("Idfichefrais");
        moisAnnee = jsonObject.optString("Moisannee");
        nbJustificatifs = jsonObject.optInt("Nbjustificatifs");
        montantValide = jsonObject.optDouble("Montantvalide");
        dateModif = jsonObject.optLong("Datemodif");
        try {
            listeLigneFraisForfait = LigneFraisForfait.jsonToArrayListObject(jsonObject.getJSONArray("Lignefraisforfaits"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            listeLigneFraisHorsForfait = LigneFraisHorsForfait.jsonToArrayListOjbect(jsonObject.getJSONArray("Lignefraishorsforfait"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static ArrayList<FicheFrais> fromJson(JSONArray jsonArray){
        ArrayList<FicheFrais> collectionFicheFrais = new ArrayList<FicheFrais>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                collectionFicheFrais.add(
                        new FicheFrais(jsonArray.getJSONObject(i))
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return collectionFicheFrais;

    }

    /*public static FicheFrais jsonToObject(JSONObject jsonObject) {

        FicheFrais ficheFrais = new FicheFrais();
        ficheFrais.idFicheFrais = jsonObject.optString("Idfichefrais");
        ficheFrais.moisAnnee = jsonObject.optString("Moisannee");
        ficheFrais.nbJustificatifs = jsonObject.optInt("Nbjustificatifs");
        ficheFrais.montantValide = jsonObject.optDouble("Montantvalide");
        ficheFrais.dateModif = jsonObject.optLong("Datemodif");
        try {
            ficheFrais.listeLigneFraisForfait = LigneFraisForfait.jsonToArrayListObject(jsonObject.getJSONArray("Lignefraisforfaits"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            ficheFrais.listeLigneFraisHorsForfait = LigneFraisHorsForfait.jsonToArrayListOjbect(jsonObject.getJSONArray("Lignefraishorsforfait"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ficheFrais;
    }*/
}
