package com.gsb.androFrais.classesMetier;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LigneFraisHorsForfait {

    private int idLigneFraisHorsForfait;
    private long date;
    private double montant;
    private String libelle;

    public int getIdLigneFraisHorsForfait() {
        return idLigneFraisHorsForfait;
    }

    public long getDate() {
        return date;
    }

    public double getMontant() {
        return montant;
    }

    public String getLibelle() {
        return libelle;
    }

    public static ArrayList<LigneFraisHorsForfait> jsonToArrayListOjbect(JSONArray jsonArray){
        ArrayList<LigneFraisHorsForfait> collectionLigneFraisHorsForfait = new ArrayList<LigneFraisHorsForfait>();
        if(jsonArray instanceof JSONArray) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    LigneFraisHorsForfait ligneFraisHorsForfait = new LigneFraisHorsForfait();
                    ligneFraisHorsForfait.idLigneFraisHorsForfait = jsonObject.optInt("Idlignefraishorsforfait");
                    ligneFraisHorsForfait.date = jsonObject.optLong("Date");
                    ligneFraisHorsForfait.montant = jsonObject.optDouble("Montant");
                    ligneFraisHorsForfait.libelle = jsonObject.optString("Libelle");
                    collectionLigneFraisHorsForfait.add(ligneFraisHorsForfait);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return collectionLigneFraisHorsForfait;
    }

}
