package com.gsb.androFrais;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    protected void authentification(View v){

        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);

        final EditText editTextLogin = (EditText) findViewById(R.id.editTextLogin);
        final EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        final String login = editTextLogin.getText().toString();
        final String password = editTextPassword.getText().toString();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://ws.gsb.com/appliFrais/Connection",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        JSONObject connected = null;
                        try {
                            connected = new JSONObject(response);
                            boolean state = connected.getBoolean("data");
                            if (state) {
                                Intent myIntent = new Intent(MainActivity.this, HomeActivity.class);
                                startActivity(myIntent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            String err = e.toString();
                            Log.e("MainActivity", "JsonException" + err);
                            ;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.toString();
                Log.e("MainActivity", err);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", login);
                params.put("mdp", password);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };//fin stringRequest
        queue.add(sr);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/*
        final Button buttonLogin = (Button) findViewById(R.id.buttonLogIn);
        final EditText editTextLogin = (EditText) findViewById(R.id.editTextLogin);
        final EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               CookieManager cookieManager = new CookieManager();
                CookieHandler.setDefault(cookieManager);

                final String login = editTextLogin.getText().toString();
                final String password = editTextPassword.getText().toString();

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                StringRequest sr = new StringRequest(Request.Method.POST,
                        "http://ws.gsb.com/appliFrais/Connection",
                        new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        JSONObject connected = null;
                        try {
                            connected = new JSONObject(response);
                            boolean state = connected.getBoolean("data");
                            if (state) {
                                Intent myIntent = new Intent(MainActivity.this, HomeActivity.class);
                                startActivity(myIntent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            String err = e.getMessage();
                            Log.e("MainActivity", "JsonException" + err);
                            ;
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String err = error.getMessage();
                        Log.e("MainActivity", err);
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("login", login);
                        params.put("mdp", password);
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        return params;
                    }
                };//fin stringRequest
                queue.add(sr);
            }
        });//fin onClickListener
       */

    }//fin méthode create
}
