package com.gsb.androFrais;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gsb.androFrais.adapter.FicheFraisAdapter;
import com.gsb.androFrais.classesMetier.FicheFrais;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ConsulterFicheFraisActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulter_fiche_frais);

        final ArrayList<FicheFrais> collectionFicheFrais = new ArrayList<FicheFrais>();
        final FicheFraisAdapter adapter = new FicheFraisAdapter(this,R.layout.item_fiche_frais,collectionFicheFrais);

        ListView listView = (ListView)findViewById(R.id.listeFicheFrais);
        listView.setAdapter(adapter);

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.GET,
                "http://ws.gsb.com/appliFrais/FicheFrais",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            collectionFicheFrais.addAll(FicheFrais.fromJson(jsonObject.getJSONArray("data")));
                            adapter.addAll(collectionFicheFrais);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String err = error.getMessage();
                Log.e("ConsulterFicheFrais", err);
            }
        }) ;//fin stringRequest
        queue.add(sr);
    }
}
