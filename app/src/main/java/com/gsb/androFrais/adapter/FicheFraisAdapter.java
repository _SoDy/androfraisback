package com.gsb.androFrais.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gsb.androFrais.R;
import com.gsb.androFrais.classesMetier.FicheFrais;

import java.util.ArrayList;
import java.util.Date;

public class FicheFraisAdapter extends ArrayAdapter<FicheFrais> {

    public FicheFraisAdapter(@NonNull Context context, int resource, @NonNull ArrayList<FicheFrais> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        FicheFrais ficheFrais = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_fiche_frais, parent, false);
        }
        if (position % 2 == 0) {
            convertView.setBackgroundColor(Color.GREEN);
        } else {
            convertView.setBackgroundColor(Color.MAGENTA);
        }

        TextView tvMoisAnnee = (TextView) convertView.findViewById(R.id.moisAnnee);
        TextView tvNbJustificatifs = (TextView) convertView.findViewById(R.id.nbJustificatifs);
        TextView tvMontantValide = (TextView) convertView.findViewById(R.id.montantValide);
        TextView tvDateModif = (TextView) convertView.findViewById(R.id.dateModif);

        tvMoisAnnee.setText(ficheFrais.getMoisAnnee());

        String nbJustificatif = Integer.toString(ficheFrais.getNbJustificatifs());
        if (nbJustificatif == "NaN") {
            nbJustificatif = "0";
        }
        tvNbJustificatifs.setText(nbJustificatif);

        String montantValide = Double.toString(ficheFrais.getMontantValide());
        if (montantValide == "NaN") {
            montantValide = "0";
        }
        tvMontantValide.setText(montantValide);

        String dateModification;
        long timeStamp = ficheFrais.getDateModif();
        if (timeStamp == 0) {
            dateModification = "Pas de modifications";
        }else {
            Date date = new Date(timeStamp);
            dateModification = date.toString();
        }
        tvDateModif.setText(dateModification);

        return convertView;
    }
}
